# Wardrobify

Team:

* Michaela Arteberry - Hats Microservice
* Sean Lim - Shoes Microservice

## Design
![two-shot-flow](two-shot-flow)
## Shoes microservice

Poller: Gets information from bins in wardrobe which we then store in the VO model
View function: Handles HTTP Methods: GET, PUT, DELETE
FrontEnd: Delete button was included on each card of shoes listed, added a form to create new shoes. Connected with backend api by using fetch

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
