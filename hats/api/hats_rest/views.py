from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import LocationVO, HatModel

# Create your views here.


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name"]

class HatModelEncoder(ModelEncoder):
    model = HatModel
    properties = ["fabric", "color", "picture", "style_name", "location", "pk"]
    encoders = {"location": LocationVOEncoder(),
                }

@require_http_methods(["GET", "POST"])
def api_list_hats(request, hat_vo_id=None):
    if request.method == "GET":
        if hat_vo_id is not None:
            hat = HatModel.objects.get(id=hat_vo_id)
        else:
            hats = HatModel.objects.all()

        return JsonResponse({"hats": hats}, HatModelEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            hat_href = content["location"]
            location = LocationVO.objects.get(import_href=hat_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Location not found."}, status=400, safe=False)

        hats = HatModel.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatModelEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_hats(request, pk):
    """
    Single-object API for the Location resource.

    GET:
    Returns the information for a Location resource based
    on the value of pk
    {
        "id": database id for the location,
        "closet_name": location's closet name,
        "section_number": the number of the wardrobe section,
        "shelf_number": the number of the shelf,
        "href": URL to the location,
    }

    PUT:
    Updates the information for a Location resource based
    on the value of the pk
    {
        "closet_name": location's closet name,
        "section_number": the number of the wardrobe section,
        "shelf_number": the number of the shelf,
    }

    DELETE:
    Removes the location resource from the application
    """
    if request.method == "GET":
        try:
            hat = HatModel.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatModelEncoder,
                safe=False
            )
        except HatModel.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = HatModel.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatModelEncoder,
                safe=False,
            )
        except HatModel.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            hat = HatModel.objects.get(id=pk)

            props = ["fabric", "color", "picture", "style_name", "location"]
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatModelEncoder,
                safe=False,
            )
        except HatModel.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
