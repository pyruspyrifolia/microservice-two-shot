import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import NewHatForm from './NewHats';
import ListHats from './ListHats';
import NewShoesForm from './NewShoesForm';
import ShoesList from './ShoesList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats/" element={<ListHats />} />
          <Route path="hats">
            <Route path="new" element={<NewHatForm />} />
            <Route index element={<ListHats />} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoesList />} />
            <Route path="new" element={<NewShoesForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
