import React from 'react';
import { useEffect, useState } from 'react';

function NewHatForm() {
    const [locations, setLocations] = useState([]);

    const [styleName, setStyleName] = useState('');
    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const [picture, setPicture] = useState('');
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.style_name = styleName;
        data.picture = picture;
        data.fabric = fabric;
        data.color = color;
        data.location = location;

        console.log(data);
        const locationUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setStyleName('');
            setFabric('');
            setColor('');
            setLocation('');
            setPicture('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);
    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Enter a new Hat!</h1>
                        <form onSubmit={handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleStyleNameChange} placeholder="Style Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Style Name</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Hat Color</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                                <label htmlFor="fabric">Fabric of Hat</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input onChange={handlePictureChange} value={picture} placeholder="Picture" required type="url" name="picture" id="picture" className="form-control" />
                                <label htmlFor="picture">Picture of Hat</label>
                            </div>

                            <div className="mb-3">
                                <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                                    <option value="">Choose a Closet</option>
                                    {locations.map(location => {
                                        return (
                                            <option value={location.href} key={location.href}>
                                                {location.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>

                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}

export default NewHatForm;
