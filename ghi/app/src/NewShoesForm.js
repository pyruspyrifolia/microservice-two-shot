import React, { useEffect, useState } from 'react';


function NewShoesForm() {
    const [bins, setstates] = useState([])

    const [modelName, setModelName] = useState('');
    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    };

    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    };

    const [manufacturer, setManufacturer] = useState('');
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    };


    const [picture, setPicture] = useState('');
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    };



    const [bin, setBin] = useState('');
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    };


    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};


        data.model_name = modelName;
        data.color = color;
        data.manufacturer = manufacturer;
        data.picture = picture;
        data.bins = bin;

        console.log(data);
        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newShoes = await response.json();
            console.log(newShoes);
            setModelName('');
            setColor('');
            setManufacturer('');
            setPicture('');
            setBin('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setstates(data.bins);

        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add New Shoes</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} placeholder="Model Name" required type="text" name="name" id="name" className="form-control" value={modelName}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color}/>
                <label htmlFor="Color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={manufacturer}/>
                <label htmlFor="Manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureChange} placeholder="Picture" required type="url" name="picture" id="picture" className="form-control" value={picture}/>
                <label htmlFor="Picture">Picture</label>
              </div>
              <div className="mb-3">
                <select onChange={handleBinChange} required name="bins" id="bins" className="form-select">
                    <option>
                        Choose a bin!
                    </option>
                {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    );
                                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
        </div>
    );
    }

export default NewShoesForm;
