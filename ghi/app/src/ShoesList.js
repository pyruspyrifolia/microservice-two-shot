import React, { useEffect, useState, fetchData } from 'react';
import { Link, } from 'react-router-dom';





const ShoesList = (props) => {
    const [shoeColumns, setShoeColumns] = useState([[], [], []]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                // Get the list of shoes
                const data = await response.json();

                // Create a list of for all the requests and
                // add all of the requests to it
                const requests = [];
                for (let shoe of data.shoes) {
                    const detailUrl = `http://localhost:8080/api/shoes/${shoe.pk}`;
                    requests.push(fetch(detailUrl));
                }

                // Wait for all of the requests to finish
                // simultaneously
                const responses = await Promise.all(requests);

                // Set up the "columns" to put the conference
                // information into
                const columns = [[], [], []];

                // Loop over the conference detail responses and add
                // each to to the proper "column" if the response is
                // ok
                let i = 0;

                for (const shoeResponse of responses) {
                    if (shoeResponse.ok) {
                        const details = await shoeResponse.json();
                        console.log(details)
                        columns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(shoeResponse);
                    }
                }

                // Set the state to the new list of three lists of
                // conferences
                setShoeColumns(columns);
                console.log(columns)
            }
        } catch (e) {
            console.error(e);
        }
    }
    const deleteShoes = async (pk) => {
        const url = `http://localhost:8080/api/shoes/${pk}`;
        try {
            const response = await fetch(url, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                },
            });

            if (response.ok) {
                fetchData();
            } else {
                console.error(response);
            }
        } catch (e) {
            console.error(e);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
    <>
            <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
                <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
                <h1 className="display-5 fw-bold">Shoes!</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">

                    </p>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">

                    </div>
                </div>
            </div>
            <div className="container">
                <h2>Shoes</h2>
                <div className="row">
                    {shoeColumns.map((shoeList, index) => {
                        return (
                            <div className="col">
                                {shoeList.map(shoe => {

                                    return (
                                        <div key={shoe.pk} className="card mb-3 shadow">
                                            <img src={shoe.picture} className="card-img-top" />
                                            <div className="card-body">
                                                <h5 className="card-title">{shoe.model_name}</h5>
                                                <h6 className="card-subtitle mb-2 text-muted">
                                                    {shoe.manufacturer}
                                                </h6>
                                                <p className="card-text">
                                                    {shoe.color}
                                                </p>

                                                <button onClick={() => deleteShoes(shoe.pk)} id="deleteBtn" className="btn btn-danger">
                                                    Delete
                                                </button>
                                            </div>
                                            <div className="card-footer">

                                            </div>
                                        </div>
                                    )})}
                            </div>
                    )})}

      </div>
      </div>
            </>
    )
                                    }



export default ShoesList;
