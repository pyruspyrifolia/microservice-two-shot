import React from 'react';
import { useEffect, useState } from "react";


const HatsList = (props) => {
    const [hatColumns, setHatColumns] = useState([[], [], []]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                // Get the list of conferences
                const data = await response.json();

                // Create a list of for all the requests and
                // add all of the requests to it
                const requests = [];
                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090/api/hats/${hat.pk}`;
                    requests.push(fetch(detailUrl));
                }

                // Wait for all of the requests to finish
                // simultaneously
                const responses = await Promise.all(requests);

                // Set up the "columns" to put the hat
                // information into
                const columns = [[], [], []];

                // Loop over the hat detail responses and add
                // each to to the proper "column" if the response is
                // ok
                let i = 0;
                for (const hatResponse of responses) {
                    if (hatResponse.ok) {
                        const details = await hatResponse.json();
                        columns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(hatResponse);
                    }
                }

                // Set the state to the new list of three lists of
                // hats
                setHatColumns(columns);
            }
        } catch (e) {
            console.error(e);
        }
    }

// Function below to delete a hat using the delete button on card.
    const deleteHat = async (pk) => {
        const url = `http://localhost:8090/api/hats/${pk}`;
        try {
            const response = await fetch(url, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                },
            });

            if (response.ok) {
                fetchData();
            } else {
                console.error(response);
            }
        } catch (e) {
            console.error(e);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <>
            <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
                <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
                <h1 className="display-5 fw-bold">WARDOBIFY!</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">
                        Need to keep track of your shoes and hats? We have
                        the solution for you!
                    </p>
                </div>
            </div>
            <div className="container">
                <h2>Hats in your Wardrobe</h2>
                <div className="row">
                    {hatColumns.map((hatList, index) => {
                        return (
                            <div className="col">
                                {hatList.map(hat => {
                                    return (
                                        <div key={hat.id} className="card mb-3 shadow">
                                            <img src={hat.picture} className="card-img-top" />
                                            <div className="card-body">
                                                <h5 className="card-title">{hat.style_name}</h5>
                                                <p className="card-text">
                                                    {hat.color} {hat.fabric}
                                                </p>
                                                <button onClick={() => deleteHat(hat.pk)} id="deleteBtn" className="btn btn-danger">
                                                    Delete me
                                                </button>
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                        );
                    })}
                </div>
            </div>
        </>
    );

}




export default HatsList;
