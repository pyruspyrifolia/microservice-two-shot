
from django.http import  JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoes, BinsVO
# Create your views here.


class BinsVODetailEncoder(ModelEncoder):
    model = BinsVO
    properties = ["name", "import_href", "id"]


class ShoeModelEncoder(ModelEncoder):
    model = Shoes
    properties = ["manufacturer", "model_name", "color", "picture", "bins", "pk"]

    encoders = {
        "bins": BinsVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()

        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeModelEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bins"]
            bins = BinsVO.objects.get(import_href=bin_href)
            content["bins"] = bins
        except BinsVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status = 400
            )

        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeModelEncoder,
            safe=False,
        )

@require_http_methods(["GET","PUT", "DELETE"])
def api_shoes(request, pk):
    if request.method == "GET":
        try:
            shoes = Shoes.objects.get(id=pk)
            return JsonResponse(
                shoes,
                encoder=ShoeModelEncoder,
                safe=False
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        try:
            content = json.loads(request.body)
            shoes = Shoes.objects.get(id=pk)

            props = ["manufacturer", "model_name", "color", "picture",]
            for prop in props:
                if prop in content:
                    setattr(shoes, prop, content[prop])
            shoes.save()
            return JsonResponse(
                shoes,
                encoder=ShoeModelEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
