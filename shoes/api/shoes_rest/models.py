from django.db import models
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist

# Create your models here.
class BinsVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

class Shoes(models.Model):

    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture = models.URLField(max_length=200)
    bins = models.ForeignKey(
        BinsVO,
        related_name = "bins",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.Model_Name

    # def get_api_url(self):
    #     return reverse("api_show_shoes", kwargs=("id":self.id))
